function! Vci()
python3 << EOF
import vim, json
from urllib import request, parse


data = vim.current.buffer

ci_data = ""
for x in data:
    ci_data+=(x + '\n')

url = 'https://gitlab.com/api/v4/ci/lint'
body = {'content': ci_data}
headers = {'content-type': 'application/json'}

body = json.dumps(body).encode('utf-8')

req = request.Request(url, data=body, headers=headers)
f = request.urlopen(req)
response = f.read()
print(response)
f.close()
EOF
endfunction
command! -nargs=0 Vci call Vci()
